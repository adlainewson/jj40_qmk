#include QMK_KEYBOARD_H


#define _QWERTY 0
#define _LOWER  1
#define _RAISE  2
#define _NAVIG  3

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
};


#define LOWER MO(_LOWER)
#define RAISE MO(_RAISE)
#define NAVIG MO(_NAVIG)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Qwerty
 * ,-----------------------------------------------------------------------------------.
 * | Esc  |   Q  |   W  |   E  |   R  |   T  |   Y  |   U  |   I  |   O  |   P  | Bksp |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * | Tab  |   A  |   S  |   D  |   F  |   G  |   H  |   J  |   K  |   L  |   ;  |Enter |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Shift|   Z  |   X  |   C  |   V  |   B  |   N  |   M  |   ,  |   .  |   /  |  "   |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * | Ctrl |NAVIG |      | Alt  |Lower |    Space    |Raise |      |      |      |      |
 * `-----------------------------------------------------------------------------------'
 */
[_QWERTY] = LAYOUT_ortho_4x12 ( \
    KC_ESC,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC,
    KC_TAB,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_ENT,
    KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_QUOT,
    KC_LCTL, NAVIG,   KC_NO,   KC_LALT, LOWER,   KC_SPC,  KC_SPC,  RAISE,   KC_NO,   KC_NO,   KC_NO,   KC_NO
),


/* Lower
 * ,-----------------------------------------------------------------------------------.
 * |   ~  |   1  |   2  |   3  |      |      |      |   _  |  +   |   (  |   )  | Del  |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |   4  |   5  |   6  |      |      |      |      |      |   {  |   }  |  |   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |   0  |   7  |   8  |   9  |      |      |      |      |    | | Home | End  |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |      |RGBTOG|RGB_MOD | Next | Vol- | Vol+ | Play |
 * `-----------------------------------------------------------------------------------'
 */
[_LOWER] = LAYOUT_ortho_4x12( \
    KC_TILD,  KC_1,    KC_2,    KC_3,    _______,  _______, _______, KC_UNDS,    KC_PLUS,    KC_LPRN, KC_RPRN, KC_DEL,
    _______,  KC_4,    KC_5,    KC_6,    _______,  _______, _______, _______,    _______,    KC_LCBR, KC_RCBR, KC_PIPE,
    KC_0,     KC_7,    KC_8,    KC_9,    _______,  _______, _______, _______,    _______,    KC_HOME, KC_END,  _______,
    _______,  _______, _______, _______, _______,  _______,  RGB_TOG, RGB_MOD,    KC_MNXT,    KC_VOLD, KC_VOLU, KC_MPLY
),

/* Raise 
 * ,-----------------------------------------------------------------------------------.
 * |   `  |   !  |   @  |   #  |  F1  |  F2  |  F3  |   -  |   =  |      |      | Bksp |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |   $  |   %  |   ^  |  F4  |  F5  |  F6  |      |      |   [  |   ]  |  \   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |   &  |   *  |      |  F7  |  F8  |  F9  |      |      |Pg Up |Pg Dn |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |  F10 |  F11 |  F12  |     | Next | Vol- | Vol+ | Play |
 * `-----------------------------------------------------------------------------------'
 */
[_RAISE] = LAYOUT_ortho_4x12( \
    KC_GRV,   KC_EXLM,   KC_AT,    KC_HASH,   KC_F1,   KC_F2,   KC_F3,   KC_MINS,   KC_EQL,   KC_NO,   KC_NO,   KC_BSPC,
    _______,  KC_DLR,    KC_PERC,  KC_CIRC,   KC_F4,   KC_F5,   KC_F6,   KC_NO,     KC_NO,    KC_LBRC, KC_RBRC, KC_BSLS,
    _______,  KC_AMPR,   KC_ASTR,  _______,   KC_F7,   KC_F8,   KC_F9,   KC_NO,     KC_NO,    KC_PGUP, KC_PGDN, _______,
    _______,  _______,   _______,  _______,   KC_F10,  KC_F11,  KC_F12,  _______,   KC_MNXT,  KC_VOLD, KC_VOLU, KC_MPLY
),

/* Navigation; just arrow keys for now
 * ,-----------------------------------------------------------------------------------.
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |      |      |      |      |      | Left | Down |  Up  | Right|      |      |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 */

[_NAVIG] = LAYOUT_ortho_4x12 ( \
    KC_A,    _______, _______, _______, _______, _______, _______, _______, _______,  _______, _______, _______,
    _______, _______, _______, _______, _______, _______, KC_LEFT, KC_DOWN, KC_UP,    KC_RGHT, _______, _______, 
    _______, _______, _______, _______, _______, _______, _______, _______, _______,  _______, _______, _______,
    _______, _______, _______, _______, _______, _______, _______, _______, _______,  _______, _______, _______
)

};

void matrix_scan_user(void) {
  // Empty
};
